
-- Table: BRIGADA_Brigadistas

INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (671, 'Pascual de Jes�s', 'Orozco', 'L�zaro', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (672, 'Moises', 'Kinil', 'Mex', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (673, 'Angel Ivan', 'Orozco', 'Lazaro', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (674, 'Alejandro', 'Alvarez', 'Alatriste', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (675, 'Eleazar', 'Deferia', 'Carrillo', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (676, 'Jose Alfredo', 'Gonzalez', 'Gerardo', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (677, 'Donato', 'Francisco', 'Hernandez', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (678, 'Francisco Javier', 'Borraz', 'Jonapa', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (679, 'Judith', 'Ramirez', 'Avenda�o', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (680, 'Praxedis', 'Sinaca', 'Colin', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (681, 'Eduardo Arnulfo', 'Sinaca', 'Chagala', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (682, 'Jose Eligio', 'Valencia', 'Molina', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (683, 'Jorge Luis', 'Gutierrez', 'Perez', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (684, 'Alejandra del Rosario', 'Cruz', 'Ramos', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (685, 'Hever', 'Vazquez', 'Gordillo', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (686, 'Gilberto', 'Arguello', 'Pazos', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (687, 'Miguel', 'de los Santos', 'Xicalhua', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (688, 'Gerardo', 'Gomez', 'Ramos', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (689, 'Daniel', 'Juarez', 'Perez', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (690, 'Baltazar', 'Reyes', 'Alcocer', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (691, 'Edwin', 'Torres', 'Chan', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (692, 'Carlos', 'Vazquez', 'Sanchez', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (693, 'Carlos', 'Sosa', 'Cervantes', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (694, 'Yolo Isis', 'Vera', 'Villanueva', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (695, 'Omar', 'Reyes', 'Escobar', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (696, 'Edgar Ivan', 'perez', 'alvarez', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (697, 'Pedro Pablo', 'Rosado', 'Guerrero', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (698, 'Vidal', 'Villalobos', 'Sanchez', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (699, 'Eduardo', 'Javier', 'Oca�a', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (700, 'Gilbert', 'Salinas', 'Perez', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (701, 'Fernando Josue', 'Velazquez', 'Vazquez', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (702, 'Melqui', 'Cruz', 'Fernandez', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (703, 'Luis Ramon', 'Rosario', 'Damian', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (704, 'Ricardo', 'Merlin', 'Dominguez', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (705, 'Valentin', 'Canul', 'Chan', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (706, 'Victor Hugo', 'Hernandez', 'Padilla', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (707, 'Erendira Maria', 'Romero', 'Berny', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (708, 'Nestor Leandro', 'Zarate', 'Trujillo', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (709, 'Marco Antonio', 'Tuz', 'Puc', 0, 0, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (710, 'Luis Enrique', 'Tuz', 'Tuz', 0, 0, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (711, 'Santos Angel', 'Tuz', 'Puc', 0, 0, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (712, 'Diana', 'Herminda', 'Villareal', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (713, 'Yolanda', 'Villanueva', 'Llante', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (714, 'Gerardo Eliseo', 'Cruz', 'Morales', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (715, 'Armando de Jesus', 'Flores', 'Ruiz', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (716, 'Gerardo', 'Cupul', 'Cano', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (717, 'Lucero Celeste', 'Torres', 'Morales', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (718, 'Juan Carlos', 'Bautista', 'Mart�nez', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (719, 'Jos� Ca�n', 'Calvillo', 'Garc�a', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (720, 'Jos� Luis', 'Flores', 'Nicanor', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (721, 'Ignacio', 'Mart�nez', 'Mart�nez', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (722, 'Jesus', 'P�ramo', '�guila', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (723, 'Eutropio', 'S�nchez', 'S�nchez', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (724, 'Juan Gabriel', 'Baquedano', 'Peralta', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (725, 'Carolina', 'Mendoza', 'S�nchez', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (726, 'Tom�s', 'Yescas', 'De Los �ngeles', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (727, 'Victor', 'Eufrasio', 'Juan', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (728, 'Irma Graciela', 'Hern�ndez', 'Vera', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (729, 'Jos� Ernesto', 'Hern�ndez', 'Ram�rez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (730, 'Abigail', 'Leandro', 'Mart�nez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (731, 'Faustino Mart�n', 'P�rez', 'Juarez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (732, 'Ulises', 'Rojano', 'Agust�n', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (733, 'Abraham', 'Rojano', '�lvarez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (734, 'Prudencio', 'Tolentino', 'Dimas', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (735, 'Jos�', 'Yescas', 'de los Angeles', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (736, 'Francisco', 'Yescas', 'De Los �ngeles', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (737, 'Jenaro', 'Reyes', 'Mart�neZ', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (738, 'Rom�n', 'Ort�z', 'Castro', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (739, 'Salvador de Jes�s', 'Urquilla', 'Climaco', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (740, 'Daniel', 'Trinidad', 'Hern�ndez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (741, 'Luz Adriana', 'S�nchez', 'Mart�nez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (742, 'Cesareo', 'P�rez', 'L�pez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (743, 'Nora Guadalupe', 'Mart�nez', 'Alonzo', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (744, 'Juan Carlos', 'Cuevas', 'Cer�n', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (745, 'Sergio', 'Rojas', 'Cruz', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (746, 'David', 'Bautista', 'Paez', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (747, 'Joaqu�n', 'Garc�a', 'Barrios', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (748, 'Javier', 'Ram�rez', 'L�pez', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (749, 'Adaya Torres', 'Sergio', 'Mario', 0, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (750, 'Ang�lica', 'Hern�ndez', 'Garc�a', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (751, 'Miriam Icela', 'Alvarado', 'Flores', 1, 1, 1);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (752, 'Esteban', 'Mart�nez', 'Mateo', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (753, 'Carmina', 'Torres', 'Cruz', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (754, 'Baltazar', 'Prieto', 'Morales', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (755, 'Cecilio', 'Jim�nez', 'Romero', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (756, 'Alfredo', 'Pedro', 'Rosales', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (757, 'Jos� Luis', 'Mart�nez', 'Hern�ndez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (758, 'Antonio', 'Mendoza', 'Angel', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (759, 'Blas', 'Avila', 'Garcia', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (760, 'Carlos', 'Morales', 'Villagran', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (761, 'Gabriel', 'Santander', 'Martinez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (762, 'Sergio', 'Casta�eda', 'Ramirez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (763, 'Jorge Abraham', 'Perez', 'Perez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (764, 'Adriana Sina�', 'Mart�nez', '�vila', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (765, 'Emma Lilia', 'Hern�ndez', 'S�nchez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (766, 'Kevin', 'Ulises', 'Jacobo', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (767, 'Dan', 'de Olarte', 'Torres', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (768, 'Rodolfo', 'Villa', 'Cano', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (769, 'Natalia', 'P�rez', 'Garc�a', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (770, 'Angel David', 'Acu�a', 'Gomez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (771, 'Uriel Alan', 'Aguirre', 'Alvarado', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (772, 'Alvaro', 'Puerta', 'Corrales', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (773, 'Jose Raymundo', 'Ballin', 'Ibarra', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (774, 'Rosa Elena', 'Cervantes', 'Ruiz', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (775, 'Maximino', 'Cortes', 'Ramos', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (776, 'Edgar', 'Castillo', 'Buendia', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (777, 'Jose Eduardo', 'Garcia', 'Molina', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (778, 'Yessenia', 'Gonzalez', 'Ponce', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (779, 'Jorge', 'Gonzalez', 'Cortes', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (780, 'Porfirio', 'Leyva', 'Mendoza', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (781, 'Gamaliel', 'Martinez', 'Solano', 0, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (782, 'Juan Carlos', 'Melendez', 'Cano', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (783, 'Josue Domingo', 'Mu�oz', 'Lara', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (784, 'Alfonso', 'Rangel', 'Cruz', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (785, 'Janet', 'Renteria', 'Bailon', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (786, 'Edgar', 'Rosales', 'Hernandez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (787, 'Lucero', 'Vazquez', 'Gomez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (788, 'David Ben', 'Dom�nguez', 'de la Cruz', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (789, 'Rom�n', 'Bacilio', 'L�pez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (790, 'Mayte', 'Ju�rez', 'Reyes', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (791, 'Josue', 'Rueda', 'Segura', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (792, 'Luis Enrique', 'Luna', 'Sida', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (793, 'Javier Pazcual', 'Garabito', 'M�ndez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (794, 'Jacobo', 'Carmona', 'Fern�ndez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (795, 'Juan de Jes�s', 'Ramos', 'Olvera', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (796, 'Norma Lucia', 'Zepahua', 'Ixmatlahua', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (797, 'Christopher Ayocuan', 'Ventura', 'Aquino', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (798, 'Eddy Edmundo', 'Alcazar', 'Rivera', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (799, 'Felipe Eleazar', 'Verdugo', 'Silva', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (800, 'Alfonso', 'Kelly', 'Hernandez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (801, 'Roger', 'Arriaga', 'Perez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (802, 'Bulmaro', 'Lopez', 'Marin', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (803, 'Francisco Javier', 'Gonzalez', 'Escobedo', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (804, 'Julio', 'Delgado', 'Alvarado', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (805, 'Uriel', 'Renteria', 'Delgado', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (806, 'Eduardo', 'Barrientos', 'Arrieta', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (807, 'Daniel Israel', 'Cab', 'Tocuch', 0, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (808, 'Carolina', 'Luna', 'Perez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (809, 'Emma Lilia', 'Hern�ndez', 'S�nchez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (810, 'Armando', 'Falfan', 'Cortes', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (811, 'Hector Gibran', 'Ochoa', 'Alvarez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (812, 'Eder', 'Leon', 'Lopez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (813, 'Eleazar', 'Martinez', 'Arredondo', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (814, 'Gloria Karina', 'perez', 'Elissetche', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (815, 'Maria de Lourdes', 'Ramirez', 'Martinez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (816, 'Luis Eduardo', 'Saenz', 'Chavez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (817, 'Gerardo', 'Sanchez', 'Garcia', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (818, 'Victor Alfonso', 'Sanchez', 'Garcia', 0, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (819, 'Krisly', 'Saucedo', 'Uuh', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (820, 'Eddy', 'Torres', 'Leon', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (821, 'Raquel', 'Espinosa', 'L�pez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (822, 'Cesar Ad�n', 'Ayala', 'Villalobos', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (823, 'Marcos Miguel', 'Garabito', 'M�ndez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (824, 'Laura Adriana', 'Escobedo', 'B�ez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (825, 'German', 'Namigtle', 'Flores', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (826, 'Guadalupe Estefany', 'V�zquez', 'Soria', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (827, 'Martin', 'Garc�a', 'Garc�a', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (828, 'Milton Eduardo', 'G�mez', 'Alc�zar', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (829, 'Jos� Mar�a', 'Berista�n', 'Ruiz', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (830, 'Oswaldo Fabi�n', 'Cruz', 'Hern�ndez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (831, 'Jorge', 'V�zquez', 'Maya', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (832, 'Gustavo Rogelio', 'Baca', 'Sanchez', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (833, 'Jesus Clemente', 'Campo', 'Ocampo', 1, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (834, 'Mayra', 'Hernandez', 'Loyo', 1, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (835, 'Erik Alonso', 'Antonio', 'Hern�ndez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (836, 'Daniel', 'S�nchez', 'Garc�a', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (837, 'Felipe Agust�n', 'Lara', 'Hern�ndez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (838, 'Diana', 'Vera', 'Castillo', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (839, 'Juan', 'Maldonado', 'Ballina', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (840, 'Paul', 'Saenz', 'Rodriguez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (841, 'Rocio', 'Apolinar', 'Flores', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (842, 'Jose', 'Castillo', 'Romero', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (843, 'Denis Enrique', 'Centeno', 'Equis', 0, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (844, 'Abraham', 'De Leon', 'Hernandez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (845, 'Julio', 'Dominguez', 'Gabriel', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (846, 'Melissa Veronica', 'Macouzet', 'Pacheco', 0, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (847, 'Edgardo', 'Morales', 'Esteban', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (848, 'Aurelio', 'Olan', 'Benitez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (849, 'Luis Miguel', 'Perez', 'Ramirez', 0, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (850, 'Oscar', 'Sanchez', 'Perez', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (851, 'Judith', 'Vazquez', 'Flores', 0, 0, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (852, 'Aline Leylani', 'Dominguez', 'Islas', 0, 1, 2);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (853, 'Dorian Ivan', 'Clemente', 'Hernandez', 1, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (854, 'Luis Adrian', 'Dominguez', 'Martinez', 0, 1, 3);
INSERT INTO BRIGADA_Brigadistas (BrigadistaID, Nombre, ApellidoPaterno, ApellidoMaterno, PuestoID, Activo, EmpresaID) VALUES (855, 'Karla Ariadne', 'Vargas', 'Acompa', 0, 1, 1);
