
-- Table: SYS_Formatos

INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (1, 'sotoBosque', 'Sotobosque', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (2, 'repoblado', 'Repoblado', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (3, 'arboladoA', 'Arbolado', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (4, 'submuestra', 'Submuestra', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (5, 'claveVegetacion', 'Clave de vegetaci�n', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (6, 'caracteristicasUPM', 'Caracter�sticas del conglomerado', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (7, 'carbonoIncendios', 'Carbono e incendios', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (8, 'longitudInterceptada', 'Longitud interceptada', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (9, 'suelo', 'Suelos', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (10, 'condicionDegradacion', 'Degradaci�n del suelo', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (11, 'erosionHidrica', 'Herosi�n h�drica', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (12, 'deformacionTerreno', 'Deformaci�n del terreno', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (13, 'observaciones', 'Observaciones', 'A');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (14, 'arboladoD', 'Arbolado', 'D');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (15, 'hojarascaProfundidad', 'Hojarasca profundidad suelo', 'E');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (16, 'muestrasPerfil', 'Muestras del perfil', 'E');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (17, 'fotografiaHemisferica', 'Fotograf�a hemisferica', 'F');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (18, 'parametrosFQ', 'Par�metros f�sico qu�micos', 'G');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (19, 'repobladoVM', 'Repoblado vegetaci�n menor', 'G');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (20, 'arboladoG', 'Arbolado', 'G');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (21, 'vegetacionMenor', 'Vegetaci�n menor', 'H');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (22, 'vegetacionMayorI', 'Vegetaci�n mayor MCMI', 'H');
INSERT INTO SYS_Formatos (FormatoID, Formulario, Formato, Modulo) VALUES (23, 'vegetacionMayorG', 'Vegetaci�n mayor MCMG', 'H');

